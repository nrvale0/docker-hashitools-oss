# build for multi-stage image
FROM ubuntu:bionic as build

ENV PATH /bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
	apt-get install wget unzip git -y

ARG TERRAFORM_VERSION=0.12.18
ARG TERRAFORM_DOWNLOAD_URI="https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip"

ARG CONSUL_VERSION=1.6.2
ARG CONSUL_DOWNLOAD_URI="https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip"

ARG VAULT_VERSION=1.3.0
ARG VAULT_DOWNLOAD_URI="https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip"

ARG NOMAD_VERSION=0.10.2
ARG NOMAD_DOWNLOAD_URI="https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip"

ARG SENTINEL_VERSION=0.13.1
ARG SENTINEL_DOWNLOAD_URI="https://releases.hashicorp.com/sentinel/${SENTINEL_VERSION}/sentinel_${SENTINEL_VERSION}_linux_amd64.zip"

RUN mkdir /tmp/hashicorp

RUN echo 'Download Terraform...'; \
	cd /tmp/hashicorp; \
	wget -nv -c -O tf.zip "${TERRAFORM_DOWNLOAD_URI}"; \
	unzip tf.zip

RUN echo 'Download Consul...'; \
	cd /tmp/hashicorp; \
	wget -nv -c -O c.zip "${CONSUL_DOWNLOAD_URI}"; \
	unzip c.zip

RUN echo 'Download Vault...'; \
	cd /tmp/hashicorp; \
	wget -nv -c -O v.zip "${VAULT_DOWNLOAD_URI}"; \
	unzip v.zip

RUN echo 'Download Nomad...'; \
	cd /tmp/hashicorp; \
	wget -nv -c -O n.zip "${NOMAD_DOWNLOAD_URI}"; \
	unzip n.zip

RUN echo 'Download Sentinel...'; \
	cd /tmp/hashicorp; \
	wget -nv -c -O s.zip "${SENTINEL_DOWNLOAD_URI}"; \
	unzip s.zip

RUN echo 'Deleting download files...'; \
	rm /tmp/hashicorp/*.zip

RUN mv /tmp/hashicorp/* /usr/local/bin/

COPY ./Dockerfile /Dockerfile

MAINTAINER Nathan Valentine
LABEL email="nrvale0@protonamil.com" \
 	repo="https://gitlab.com/nrvale0/docker-hashitools-oss"
